---
layout: post
title: Satellite
description: Page for satellite mapping
image: assets/images/satellite.png
nav-menu: true
---

Climate mapping is the representation of the weather patterns in a designated area.  Our goal is to provide a distribution of the weather on a hot summer day in New York City; weather on such a day can vary up to 20 degrees due to inconsistencies in greenery and land surface temperatures.  Urban microclimates have evolved very much over time, and our collected data will provide us with more insight. 


The script used for this project utilizes Landsat datasets to portray a single scene from the USGS archives.  With a specific longitude, latitude, start and end dates, and the amount of cloud cover established into the script, a map of the region requested should be plotted, providing differences in land surface temperature from the function established in the script. The original script was edited to portray a latitude of 40.730610, and a longitude of -73.935242 (coordinates for South Bronx); in addition, the dates used in the script were from July 1st, 2021 to September 1st, 2021.  The cloud cover is zero to get the most accurate depiction of land surface temperature.   